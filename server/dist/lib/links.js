'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _shorthash = require('shorthash');

var _shorthash2 = _interopRequireDefault(_shorthash);

var _slugify = require('slugify');

var _slugify2 = _interopRequireDefault(_slugify);

var _db = require('./db.js');

var _db2 = _interopRequireDefault(_db);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var hasSlug = function hasSlug(slug) {
    var record = void 0;

    if (record = _db2.default.retrieve(slug)) {
        _db2.default.update(slug, 'visits', record.visits + 1);
        return record.destination;
    }
    return false;
};

var createSlugfromURL = function createSlugfromURL(url) {
    return _shorthash2.default.unique(url);
};

var shorten = function shorten(url, alias) {
    var slug = void 0;

    if (alias) {
        slug = (0, _slugify2.default)(alias).toLowerCase();
    } else {
        slug = createSlugfromURL(url);
    }

    // If slug exists
    if (_db2.default.retrieve(slug)) {
        // If an alias is provided and slug exists in database send error. 
        // If an alias has not been provided send back the existing record for the URL.
        if (alias) {
            return false;
        } else {
            return slug;
        }
    } else {
        _db2.default.save(slug, url, Math.floor(new Date() / 1000));
        return slug;
    }
};

exports.default = { shorten: shorten, hasSlug: hasSlug };