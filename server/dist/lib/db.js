'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _lowdb = require('lowdb');

var _lowdb2 = _interopRequireDefault(_lowdb);

var _FileSync = require('lowdb/adapters/FileSync');

var _FileSync2 = _interopRequireDefault(_FileSync);

var _config = require('../app/config.js');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var adapter = new _FileSync2.default(_config2.default.dbFile);
var ldb = (0, _lowdb2.default)(adapter);

// set defaults for database
ldb.defaults({ links: [], lastModified: '' }).write();

var retrieve = function retrieve(slug) {
    return ldb.get('links').find({ slug: slug }).value();
};

var save = function save(slug, url, time) {
    ldb.get('links').push({
        slug: slug,
        destination: url,
        createdAt: time,
        visits: 0
    }).write();
    ldb.set('lastModified', time).write();
};

var update = function update(slug, key, value) {
    var obj = {};

    obj[key] = value;

    return ldb.get('links').find({ slug: slug }).assign(obj).write();
};

exports.default = { retrieve: retrieve, save: save, update: update };