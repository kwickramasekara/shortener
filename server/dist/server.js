'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _config = require('./app/config.js');

var _config2 = _interopRequireDefault(_config);

var _links = require('./lib/links.js');

var _links2 = _interopRequireDefault(_links);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var server = (0, _express2.default)();

var apiRequest = function apiRequest(req) {
    var serverBaseURL = req.protocol + '://' + req.get('host') + _config2.default.appDir + '/';
    var response = {};

    if (req.query.action) {
        if (req.query.action == 'shorten') {
            if (req.query.url) {
                if (req.query.alias) {
                    var result = _links2.default.shorten(req.query.url, req.query.alias);
                    if (result) {
                        response.error = false;
                        response.data = serverBaseURL + result;
                    } else {
                        response.error = true;
                        response.data = 'Slug exists';
                    }
                } else {
                    var _result = _links2.default.shorten(req.query.url);

                    if (_result) {
                        response.error = false;
                        response.data = serverBaseURL + _result;
                    } else {
                        response.error = true;
                        response.data = 'Slug exists';
                    }
                }
            } else {
                response.error = true;
                response.data = 'No URL specified';
            }
        }
    } else {
        response.error = true;
        response.data = 'No action specified';
    }

    return response;
};

server.set('views', './client/src');
server.set('view engine', 'pug');
server.use(_express2.default.static(_path2.default.join(__dirname, '../../client/dist')));

server.get('/', function (req, res) {
    res.render('index', {
        appDir: _config2.default.appDir
    });
});

server.get('/api', function (req, res) {
    res.send(apiRequest(req));
});

server.get('*', function (req, res) {
    var result = void 0;

    if (result = _links2.default.hasSlug(req.originalUrl.substring(1))) {
        res.redirect(result);
    } else {
        res.send('Error: 404. Not found.');
    }
});

server.listen(_config2.default.port, function () {
    console.info('Listening on port', _config2.default.port);
});