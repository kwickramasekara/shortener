import express from 'express';
import config from './app/config.js';
import links from './lib/links.js';
import path from 'path';

const server = express();

const apiRequest = ( req ) => {
    let serverBaseURL = req.protocol + '://' + req.get( 'host' ) + config.appDir + '/';
    let response = {};

    if ( req.query.action ) {
        if ( req.query.action == 'shorten' ) {
            if ( req.query.url ) {
                if ( req.query.alias ) {
                    let result = links.shorten( req.query.url, req.query.alias );
                    if ( result ) {
                        response.error = false;
                        response.data = serverBaseURL + result;
                    }
                    else {
                        response.error = true;
                        response.data = 'Slug exists';
                    }
                }
                else {
                    let result = links.shorten( req.query.url );

                    if ( result ) {
                        response.error = false;
                        response.data = serverBaseURL + result;
                    }
                    else {
                        response.error = true;
                        response.data = 'Slug exists';
                    }
                }
            }
            else {
                response.error = true;
                response.data = 'No URL specified'
            }
        }
    } else {
        response.error = true;
        response.data = 'No action specified'
    }

    return response;
};

server.set( 'views', './client/src' );
server.set( 'view engine', 'pug' );
server.use( express.static( path.join( __dirname, '../../client/dist' ) ) );

server.get( '/', ( req, res ) => {
	res.render( 'index', {
        appDir: config.appDir
    } );
});

server.get( '/api', ( req, res ) => {
	res.send( apiRequest( req ) );
});

server.get( '*', ( req, res ) => {
    let result;
    
    if ( result = links.hasSlug( req.originalUrl.substring(1) ) ) {
        res.redirect( result );
    }
    else {
        res.send( 'Error: 404. Not found.' ); 
    }
});

server.listen( config.port, () => {
	console.info( 'Listening on port', config.port );
});