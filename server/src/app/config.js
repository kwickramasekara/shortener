const env = process.env;

export default {
  port: env.PORT || 4545,
  host: env.HOST || '0.0.0.0',
  dbFile: './server/db.json',
  appDir: env.APP_DIR || '' // subdirectory where the app llves. Used for generating links
};
