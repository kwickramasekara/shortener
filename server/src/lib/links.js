import sh from 'shorthash';
import slugify from 'slugify';
import db from './db.js';

const hasSlug = ( slug ) => {
    let record;

    if ( record = db.retrieve( slug ) ) {
        db.update( slug, 'visits', record.visits + 1 );
        return record.destination;
    }
    return false;
};

const createSlugfromURL = ( url ) => {
    return sh.unique( url );
};

const shorten = ( url, alias ) => {
    let slug;

    if ( alias ) {
        slug = slugify( alias ).toLowerCase();
    }
    else {
        slug = createSlugfromURL( url );
    }

    // If slug exists
    if ( db.retrieve( slug ) ) {
        // If an alias is provided and slug exists in database send error. 
        // If an alias has not been provided send back the existing record for the URL.
        if ( alias ) {
            return false;
        } else {
            return slug;
        }
    } 
    else {
        db.save( slug, url, Math.floor( new Date() / 1000 ) );
        return slug;
    }
};

export default { shorten, hasSlug };