import lowdb from 'lowdb';
import FileSync from 'lowdb/adapters/FileSync';
import config from '../app/config.js';

const adapter = new FileSync( config.dbFile );
const ldb = lowdb(adapter);

// set defaults for database
ldb.defaults({ links: [], lastModified: '' })
  .write()

const retrieve = ( slug ) => {
    return ldb.get( 'links' ).find( { slug: slug } ).value();
};

const save = ( slug, url, time ) => {
    ldb.get( 'links' ).push({
        slug: slug,
        destination: url,
        createdAt: time,
        visits: 0
    }).write();
    ldb.set( 'lastModified', time ).write();
};

const update = ( slug, key, value ) => {
    let obj = {};

    obj[key] = value;

    return ldb.get( 'links' ).find({ slug: slug }).assign( obj ).write();
};

export default { retrieve, save, update }