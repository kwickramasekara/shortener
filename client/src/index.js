import App from './components/app.js';
import { h, render } from 'preact';
import './styles/skeleton.css';
import './styles/app.css';

render( <App />, document.body );