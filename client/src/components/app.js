import { h, Component } from 'preact';
import axios from 'axios';
import Result from './result.js';

export default class App extends Component {
	constructor() {
        super();
        // set initial time:
        this.state.loading = false;
	}
	
	onClick() {
		this.setState( { loading: true } );

		axios( subPath + '/api', {
			params: {
				'action': 'shorten',
				'url': this.state.destinationURL,
				'alias': this.state.alias
			}
		}).then( resp => {
			if ( resp.data ) {
				this.setState({
					error: resp.data.error,
					errorText: ( resp.data.error ) ? resp.data.data : false,
					shortURL: ( ! resp.data.error ) ? resp.data.data : false,
					loading: false
				});
			}
		});
	}

	onURLChange( e ) {
		this.setState( { destinationURL: e.target.value } );
	}

	onAliasChange( e ) {
		this.setState( { alias: e.target.value } );
	}

	render() {
		return (
			<div class="app">
				<div class="container">
					<div class="row">
						<div class="twelve columns header">
							<img src="images/scissors.png" width="50"/>
							<h1>Shortener</h1>
							<i>Make long URLs short</i>
						</div>
					</div>

					<div class="row">
						<div class="six columns main">
							<label>Full URL:</label>
							<input class="u-full-width" type="text" placeholder="http://google.com" onChange={e => this.onURLChange( e )} value={this.state.destinationURL} />
							<label>Alias (optional):</label>
							<input type="text" placeholder="short" onChange={e => this.onAliasChange( e )} value={this.state.alias} />
							<button class="button-primary u-full-width" onClick={this.onClick.bind(this)}>{ this.state.loading  ? '...' : 'Shorten' }</button>
							{ ! this.state.loading ? <Result error={this.state.error} errorText={this.state.errorText} shortURL={this.state.shortURL} /> : null }
						</div>
					</div>
				</div>
			</div>
		);
	}
}