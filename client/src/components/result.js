import { h, Component } from 'preact';

export default class Result extends Component {
    render() {
        if ( this.props.error ) {
            return (
                <span>{this.props.errorText}</span>
            );
        } 
        else {
            return (
                <a href={this.props.shortURL} target="_blank">{this.props.shortURL}</a>
            );
        }
		
	}
}