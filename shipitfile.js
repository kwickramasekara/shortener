// shipitfile.js
module.exports = shipit => {
    // Load shipit-deploy tasks
    require('shipit-deploy')(shipit)
    require('shipit-shared')(shipit)

    const SERVER = 'keithw.me';
    const SERVER_DEPLOY_TO_DIR = '/var/www/shortener';
    const FOREVER_SCRIPT = 'server/dist/server.js';
    const FOREVER_ID = 'shortener'; // helpful to identify processes
    const GIT_REPO = 'https://gitlab.com/kwickramasekara/shortener.git';
  
    shipit.initConfig({
      default: {
        deployTo: SERVER_DEPLOY_TO_DIR,
        repositoryUrl: GIT_REPO,
        keepReleases: 2,
        shared: {
            files: ['server/db.json'],
            overwrite: true
        },
      },
      production: {
        servers: {
            host: SERVER,
            user: 'root',
        }
      },
    });

    shipit.blTask('app:build', async () => {
        await shipit.remote(`cd ${shipit.releasePath} && npm install && npm run build`);
    });

    shipit.blTask('app:restart', async () => {
        await shipit.remote(`cd ${shipit.currentPath} && APP_DIR=/s forever restart ${FOREVER_ID}`).catch( async ({ stderr }) => {
            // forever throws error if it doesnt have the script running to restart
            if ( stderr ) {
                await shipit.remote(`cd ${shipit.currentPath} && APP_DIR=/s forever start --id "${FOREVER_ID}" ${FOREVER_SCRIPT}`);
            }
        } );
    });

    shipit.on('updated', () => {
        // build app after pushing to environment & before symlinking current folder
        shipit.start('app:build');
    });

    shipit.on('deployed', () => {
        // forever restart app
        shipit.start('app:restart');
    });
  }