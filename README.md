# Shortener

A URL shortener built with [Express](http://expressjs.com), [Preact](https://preactjs.com)(lightweight alternative to React) and [Lowdb](https://github.com/typicode/lowdb)(JSON database).

![Screenshot of the blog](screenshot.png)

## Getting started

1. `npm install`
2. `npm server`
3. `start client`
4. Open `http://localhost:4545`

