var path = require('path');

module.exports = {
	// entry file - starting point for the app
	entry: './client/src/index.js',

	// where to dump the output of a production build
	output: {
		path: path.join( __dirname, 'client/dist' ),
		filename: 'bundle.js'
	},

	module: {
		rules: [
			{
				test: /\.jsx?/i,
				loader: 'babel-loader',
				options: {
					presets: [
						'es2015'
					],
					plugins: [
						['transform-react-jsx', { pragma: 'h' }]
					]
				}
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            }
		]
	}
};